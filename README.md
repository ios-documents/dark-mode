
# Dark Mode Guidelines


![](https://gitlab.com/ios-documents/dark-mode/-/raw/main/Images/DarkMode.jpeg?inline=false)

Dark Mode was introduced in iOS 13 and announced at WWDC 2019. It provides a darker theme to iOS and allows you to do the same for your app. Most of the users like to use a dark theme and It’s a great addition to give to your users so they can experience your app in a darker design.


## Convention

The use of the color palette is mandatory for all sites and application. It is therefore forbidden to use any other colors than those proposed.  

The color palette of the State Design System consists of two parts: the "Unique" palette and the "Variant" palette.

- A "unique" color means that regarding the background, dark or light mode, this color will not changed.

- A "variant" color means that regarding the background, dark or light mode, this color will automatically changed.

## Naming

When you create a new color this one should match the following pattern `main color - name`
##### Example  `red-oshisan`, `blue-arkana`, `yellow-banana`, `blue-azea`

When you create a new variant this one should match the following pattern `light color name - dark color name`
##### Example  `oshisan-arkana`, `azea-yellow`, `azea-red`



|  Variant name| Light color name | Dark color name |
|--------------|------------------|-----------------|
|  red-oshisan |    	red		  | 	oshisan     |
|  azea-yellow |   		azea	  | 	yellow      |
|  azea-red    |   		azea	  |    	red         |




The color name is up to you, you can use this [website](https://www.color-name.com ) to help you.


##### Here is the list of the main color.

- ![](https://via.placeholder.com/15/FF0000/?text=+) Red - #FF0000
- ![](https://via.placeholder.com/15/FF500/?text=+) Orange - #FF500
- ![](https://via.placeholder.com/15/FFFF00/?text=+) Yellow - #FFFF00
- ![](https://via.placeholder.com/15/008000/?text=+) Green - #008000
- ![](https://via.placeholder.com/15/0000FF/?text=+) Blue - #0000FF
- ![](https://via.placeholder.com/15/4B0082/?text=+) Indigo - #4B0082
- ![](https://via.placeholder.com/15/EE82EE/?text=+) Violet - #EE82EE
- ![](https://via.placeholder.com/15/800080/?text=+) Purple - #800080
- ![](https://via.placeholder.com/15/FFC0CB/?text=+) Pink - #FFC0CB
- ![](https://via.placeholder.com/15/C0C0C0/?text=+) Silver - #C0C0C0
- ![](https://via.placeholder.com/15/FFD700/?text=+) Gold - #FFD700
- ![](https://via.placeholder.com/15/F5F5DC/?text=+) Beige - #F5F5DC
- ![](https://via.placeholder.com/15/A52A2A/?text=+) Brown - #A52A2A
- ![](https://via.placeholder.com/15/808080/?text=+) Grey - #808080
- ![](https://via.placeholder.com/15/000000/?text=+) Black - #000000
- ![](https://via.placeholder.com/15/FFFFFF/?text=+) White - #FFFFFF


## Example 

![](https://gitlab.com/ios-documents/dark-mode/-/raw/main/Images/example.png?inline=false)
